var mysql      = require('mysql');

//remote connection
/*
var connection = mysql.createConnection({
  host     : 'mysql.omarplummer.com',
  user     : 'codesenberg',
  password : 'qTW4D76xZFhnprER',
  database : 'codesenberg_employee_admin',
  port: '3306'
});*/

//local connection
var connection = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : 'root',
  database : 'codesenberg_employee_admin',
  port: '8889'
});

// BASE SETUP
// =============================================================================

// call the packages we need
var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var corser = require("corser");
var corserRequestListener = corser.create({
    endPreflightRequests: false
});

//handle configure for CORS
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, accept, access-control-allow-origin');

  // Route req and res through the request listener.
  corserRequestListener(req, res, function () {
        if (req.method === "OPTIONS") {
            // End CORS preflight request.
            res.writeHead(204);
            res.end();
        } else {
            // Implement other HTTP methods.
            next();
        }
    });
});

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

var port = process.env.PORT || 8080;        // set our port

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router();              // get an instance of the express Router

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function(req, res) {
    res.json({ message: 'hooray! welcome to our api!' });   
});

//login
router.route('/login')
    .post((req, res) => {
        let query = `SELECT * FROM \`employees\` WHERE \`fname\` LIKE '${req.body.fname}'`;
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                if (results.length > 0) {
                    res.send({response: 'ok', user: results[0]});
                } else {
                    res.send({response: 'fail'});
                }
            }
        });
    });

//Get all employee data or create employees
router.route('/employees')
    //get all employee data
    .get((req, res) => {
        let query = 'SELECT * FROM `employees`';
        
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                res.send(results);
            }
        });
    })
    //add new employee
    .post((req, res) => {
        let startOfQuery = 'INSERT INTO `codesenberg_employee_admin`.`employees` (`id`, `fname`, `lname`, `position`, `privileges`) ';
        let endOfQuery = `VALUES (NULL, '${req.body.fname}', '${req.body.lname}', '${req.body.position}', '${req.body.privileges}');`;
        let query = startOfQuery + endOfQuery;
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                res.send({response:'ok', employeeId: results.insertId});
            }
    });
});

//Update employee data
router.route('/employees/:id')
    .put((req, res) => {
        let startOfQuery = 'UPDATE `codesenberg_employee_admin`.`employees` SET ';
        let middleQuery = '';
        let endOfQuery = `WHERE \`employees\`.\`id\` = ${req.params.id};`;
        Object.keys(req.body).map((key, index) => {
            if (key !== 'id') {
                let comma = (index < Object.keys(req.body).length - 1) ? ', ' : ' ';
                middleQuery += `\`${key}\` = '${req.body[key]}'${comma}`;
            }
        });
        let query = startOfQuery + middleQuery + endOfQuery;
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                res.send({response:'ok'});
            }
        });
    })
    .delete((req, res) => {
        let query = `DELETE FROM \`codesenberg_employee_admin\`.\`employees\` WHERE \`employees\`.\`id\` = ${req.params.id}`;
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                res.send({response:'ok'});
            }
        });
    });

router.route('/review')
    //complete a new review or assign a new review
    .post((req, res) => {
    let startOfQuery = 'INSERT INTO `codesenberg_employee_admin`.`reviews` (`id`, `employeeId`, `review`, `reviewerId`, `status`) ';
    let endOfQuery = `VALUES (NULL, '${req.body.employeeId}', '${req.body.review}', '${req.body.reviewerId}', '${req.body.status}');`;
    let query = startOfQuery + endOfQuery;
    connection.query(query, (error, results, fields) => {
        if (error) {
            res.send(error);
        } else {
            res.send({response:'ok'});
        }
    });
});

//Retrieve/update reviews endpoint
router.route('/reviews/:employeeId')
    //Get reviews for employee
    .get((req, res) => {
        let conditional = (req.query.status) ? ` AND \`status\` =  '${req.query.status}';` : ';';
        let query = `SELECT id, review, reviewerId  FROM \`reviews\` WHERE \`employeeId\` = ${req.params.employeeId}` + conditional;
        console.log(query, 'what is this??');
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                res.send(results);
            }
        })
    })
    .put((req, res) => {
    let query = `UPDATE \`codesenberg_employee_admin\`.\`reviews\` SET \`review\` = '${req.body.review}' WHERE \`reviews\`.\`id\` = ${req.body.id}`;
        connection.query(query, (error, results, fields) => {
            if (error) {
                res.send(error);
            } else {
                res.send(results);
            }
        });
    });

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);