# Admin Demo App
Demo Admin App build with ReactJS for javascript framework, Redux for client side data storage and cross component communication, React Router for switching views, ExpressJS to act as an API server, MySQL for 
remote data storage. Typescript was used for transpiler, creating data model interfaces and strong typing.

## Contains

- [x] [Typescript](https://www.typescriptlang.org/) 2.4.1
- [x] [React](https://facebook.github.io/react/) 15.6
- [x] [Redux](https://github.com/reactjs/redux) 3.7
- [x] [React Router](https://github.com/ReactTraining/react-router) 4.1
- [x] [Redux DevTools Extension](https://github.com/zalmoxisus/redux-devtools-extension)
- [x] [Express Server]()
- [x] [Node MySQL Plugin]()


### Build tools

- [x] [Webpack](https://webpack.github.io) 3.0
  - [x] [Tree Shaking](https://medium.com/@Rich_Harris/tree-shaking-versus-dead-code-elimination-d3765df85c80)
  - [x] [Webpack Dev Server](https://github.com/webpack/webpack-dev-server)
- [x] [Awesome Typescript Loader](https://github.com/s-panferov/awesome-typescript-loader)
- [x] [PostCSS Loader](https://github.com/postcss/postcss-loader)
  - [x] [CSS next](https://github.com/MoOx/postcss-cssnext)
  - [x] [CSS modules](https://github.com/css-modules/css-modules)
- [x] [React Hot Loader](https://github.com/gaearon/react-hot-loader)
- [x] [ExtractText Plugin](https://github.com/webpack/extract-text-webpack-plugin)
- [x] [HTML Webpack Plugin](https://github.com/ampedandwired/html-webpack-plugin)


## Setup

```
$ git clone https://codesenberg@bitbucket.org/codesenberg/codesenberg-employee-review-app.git
$ cd codesenberg-employee-review-app
$ npm install
```

You will need to have mysql running locally. You will need to create the DB: `codesenberg_employee_admin` then you can import sample.db.sql

Once you have your mysql server up and running, open up the api-server.js config file and update the connections (line 14)

## Running
```
//start api server; remember to update configs to match your local mysql server setting
$ npm run api-server

//start web server
$ npm start
```

## Build

```
$ npm run build
```

## User Flow

To test this app, please start off on the login screen: http://localhost:3000/login
Admin Login: kakashi
Employee Login: naruto

When using an admin user, once you view a specific user, you will have all the options to add a new user, update a current user, create a new review, assign review or view the current user's review, or delete a user

When logging in as an employee, you will only be able to see your current employee info and you will have a link to the pending reviews (currently not hooked up to api).

## Considerations
-right now the add/delete users is not updating the redux store; implementing this would be helpful to keep the front end in sync with the backend changes
-creating a proper login with authentication and storing the user's current session, so they dont have to log in again
-adding unit testing to test frontend and back end logic
-adding proper form validation as well as disabling buttons on form submission
-standarding all the api response and make them more formal with correct error codes; plus masking mysql errors
-styling and using a library such as bootstrap to help give the UI a nice look and feel


# License

MIT
