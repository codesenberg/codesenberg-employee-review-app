import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { ApiHandlerActions, EmployeesActions, LoginActions} from '../../components';
import { RootState } from '../../root-reducer';
import * as style from './style.css';

declare namespace App {
  export interface Props extends RouteComponentProps<void> {
    apiActions: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions?: typeof LoginActions;
  }
}

@connect(mapStateToProps, mapDispatchToProps)
export class App extends React.Component<App.Props, {}> {
  constructor(props?: App.Props, context?: any) {
    super(props, context);
    //handles redirecting back to login if user isn't logged in else it will redirect back to user's logged in default page
    console.log(this.props.loginState, 'what is this??');
    if (this.props.loginState === undefined || this.props.loginState.user === undefined) {
      this.props.history.push('/login');
    } else {
      if (this.props.loginState.user.privileges === 'admin') {
          this.props.history.push('/admin');
      } else {
          this.props.history.push(`/employee/${this.props.loginState.user.id}`);
      }
    }
  }

  render() {
    return null;
  }
}

function mapStateToProps(state: RootState) {
  return {
    apiState: state.apiHandlerReducer,
    employeesState: state.employeesReducer
  };
}

function mapDispatchToProps(dispatch) {
  return {
    apiActions: bindActionCreators(ApiHandlerActions as any, dispatch),
    employeeActions: bindActionCreators(EmployeesActions as any, dispatch),
  };
}
