/* Utitilty class which checks if employee and user data has been loaded into redux store, or if it exist in cookiees
 *
 */

import { find as _find, get as _get} from 'lodash';
import * as Cookies from 'universal-cookie';

function verifyStoredData(props: StorageHandlerParams): StorageHandler {
    const cookies = new Cookies();

    //determines if current logged in user has admin priveleges
    let user = _get(props, 'loginState[0].user', undefined);

    if (user === undefined) {
      if (cookies.get('cea-user')) {
        user = cookies.get('cea-user').user;
        //check if there is a stored user in the cookies then add it to the redux store
        props.loginActions.loginUser(user);
      } else {
        //if no user, redirect back to login
        props.history.push(`/login`);
      }
    }

    //retrieve employees data from store
    let employees = _get(props, 'employeesState[0].employees.employees');
    if (employees === undefined) {
      if (cookies.get('cea-employees')) {
        //check if there is a stored employees in the cookies then add it to the redux store
        employees = cookies.get('cea-employees').employees;
        props.employeeActions.storeEmployees(employees);
      } else {
        //since no users loaded, redirect back to login screen to login in and reload users
        props.history.push(`/`);
      }
    }

    if (employees.length === 0 && props.location.pathname !== '/admin') {
      if (user.privileges === 'admin') {
        //this covers scenario where there may be no users but admin user can be redirected to admin panel to add users
        props.history.push(`/admin`);
      }
    }

    const isAdmin = (_get(user, 'privileges', 'employee') === 'admin');

    return {
      user,
      employees,
      isAdmin
    };
}

export {verifyStoredData};