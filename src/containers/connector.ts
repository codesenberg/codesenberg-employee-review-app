/* Utitilty class which exports redux connector methods so they can be easily invoked across other components
 * that need to communicate with the redux store
 */

import { bindActionCreators } from 'redux';
import { ApiHandlerActions, EmployeesActions, LoginActions} from '../components';
import { RootState } from '../root-reducer';

function mapStateToProps(state: RootState) {
  return {
    apiState: state.apiHandlerReducer,
    employeesState: state.employeesReducer,
    loginState: state.loginReducers
  };
}

function mapDispatchToProps(dispatch) {
  return {
    apiActions: bindActionCreators(ApiHandlerActions as any, dispatch),
    employeeActions: bindActionCreators(EmployeesActions as any, dispatch),
    loginActions: bindActionCreators(LoginActions as any, dispatch)
  };
}

export {mapStateToProps, mapDispatchToProps};