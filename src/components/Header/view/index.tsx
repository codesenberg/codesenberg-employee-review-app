import * as React from 'react';
const styles = require('../styles/_styles.scss');

declare namespace Header {

  export interface Props {
    title: string;
  }
  /*
  export interface State {
  }*/
}

export class Header extends React.Component<Header.Props, {}> {
  constructor(props?: Header.Props, context?: any) {
    super(props, context);

    this._configBindings(this);
    this.state = {
      title: ''
    };
  }

  /*
  This screen will render the header
  */
  public render(): any {
    return (
      <h4 className={styles.header}>Codesenberg Employee App - {this.props.title}</h4>
    );
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
  }
}
