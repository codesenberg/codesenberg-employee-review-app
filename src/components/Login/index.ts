export { loginReducers } from './reducers';
import * as LoginActionsRef from './actions';
export const LoginActions = LoginActionsRef;
export { Login } from './view';