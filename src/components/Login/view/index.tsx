import * as React from 'react';
import { Button, OverlayTrigger, Tooltip} from 'react-bootstrap';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import * as Cookies from 'universal-cookie';
import { ApiHandler, ApiHandlerActions, EmployeesActions, LoginActions} from '../..';
import * as connectors from '../../../containers/connector';
const globalStyles = require('../../../containers/App/global.scss');
const styles = require('../styles/_styles.scss');

declare namespace Login {
  export interface Props extends RouteComponentProps<void> {
    apiActions?: typeof ApiHandlerActions;
    employeeActions?: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState?: EmployeesState;
    loginActions?: typeof LoginActions;
  }

  export interface State {
    status: string;
    statusType: string;
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class Login extends React.Component<Login.Props, Login.State> {
  private _cookies = new Cookies();
  private _employeeId: number;
  private _textInput: HTMLTextAreaElement;
  private _employee: any;
  private _canEdit: boolean;
  private _addReview: boolean = false;
  private _userName: string;
  private _tooltip: typeof Tooltip;
  private _employees = [{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'frontend',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'naruto',
      lname: 'uzamaki',
      position: 'backend',
      privileges: 'employee'
    },
    {
      id: 2,
      fname: 'sasuke',
      lname: 'uchiha',
      position: 'devops',
      privileges: 'employee'
    }, {
      id: 3,
      fname: 'sakura',
      lname: 'haruno',
      position: 'qa',
      privileges: 'employee'
    }
  ];

  private _positions = {
    frontend: 'Frontend Engineer',
    backend: 'Backend Engineer',
    devops: 'Dev Ops',
    qa: 'QA',
    manager: 'Manager'
  };

  private _reviewList = [{
    id: 0,
    reviewerId: 0,
    review: 'hello world 0'
  }, {
    id: 1,
    reviewerId: 2,
    review: 'hello world 1'
  }];

  constructor(props?: Login.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    this.state = {
      status: '',
      statusType: ''
    };
    this._tooltip = (
      <Tooltip id="tooltip">You may enter any first name, to log in as that user.</Tooltip>
    );
  }

  //for now this will simulate logging by finding the first matching first name in the employees table and returning that user
  public render(): any {
    return (
        <div className={styles['login-container']}>
            <div className={['center-block', styles['login-inner']].join(' ')}>
              <div>
                <h3 className="text-center">Codesenberg</h3>
                <hr className={['text-center', globalStyles.hr].join(' ')}/>
                <h3 className="text-center">Employee App</h3>
                <input name="fname" type="text" className={styles.fname} onChange={this._storeName} placeholder={'Username'} />
                <OverlayTrigger placement="right" overlay={this._tooltip}>
                  <Button className={styles['btn-question-mark']} bsStyle="default">?</Button>
                </OverlayTrigger>
                <br/>
                <div className={[this.state.statusType, globalStyles['clear-both']].join(' ')} >{this.state.status}</div>
                <Button className={styles['btn-custom']} onClick={this._login} bsStyle="primary">Log in</Button>
              </div>
            </div>
        </div>
    );
  }

  private _storeName(e: any): void {
    this._userName = e.target.value;
  }

  private _login(): void {
    ApiHandler.request('http://localhost:8080/api/login', 'POST', {fname: this._userName}, {'Content-Type': 'application/json'}).subscribe((resp) => {
      //checks if user has been logged in
      if (resp.response === 'ok') {
        this.setState({status: 'Login successfful, loading app...', statusType: ''});
        this.props.loginActions.loginUser({user: resp.user});
        this._cookies.set('cea-user', {user: resp.user});
        ApiHandler.request('http://localhost:8080/api/employees', 'GET').subscribe((employees) => {
          this._cookies.remove('cea-employees');
          this._cookies.set('cea-employees', {employees});
          //now that user has been logged in, load user data
          this.props.employeeActions.storeEmployees({employees});
          //navigate to admin page or employee page based on user privileges
          if (resp.user.privileges === 'admin') {
              this.props.history.push('/admin');
          } else {
             this.props.history.push(`/employee/${resp.user.id}`);
          }
        });

      } else {
        this.setState({status: 'Login failed, please enter valid first name', statusType: globalStyles.error});

      }
    });
  }

  private _configBindings(scope: any): void {
    this._login = this._login.bind(scope);
    this._storeName = this._storeName.bind(scope);
  }
}