import { createAction } from 'redux-actions';
import * as Actions from '../constants';

export const loginUser = createAction<LoginState>(Actions.LOGIN_USER);
