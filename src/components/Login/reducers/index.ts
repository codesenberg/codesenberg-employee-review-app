import { handleActions } from 'redux-actions';
import * as Actions from '../constants';

const initialState: LoginState[] = [{
  user: undefined
  //MOCK state
  /*
  user: {
    id: 1,
    fname: 'Kakashi',
    lname: 'Hatake',
    position: 'manager',
    privileges: 'admin'
  }*/
}];

export const loginReducers = handleActions<LoginState[], LoginState>({
  [Actions.LOGIN_USER]: (state, action) => {
    return [{
      user: action.payload.user
    }];
  }
}, initialState);