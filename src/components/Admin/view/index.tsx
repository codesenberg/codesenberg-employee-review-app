import { get as _get } from 'lodash';
import * as React from 'react';
import { Button, Table } from 'react-bootstrap';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandlerActions, EmployeesActions, Header, LoginActions} from '../..';
import * as connectors from '../../../containers/connector';
import * as storageHandler from '../../../containers/storageHandler';
const styles = require('../styles/_styles.scss');

declare namespace Admin {
  export interface Props extends RouteComponentProps<any> {
    apiActions?: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions: typeof LoginActions;
  }

  export interface State {
    status: string;
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class Admin extends React.Component<Admin.Props, Admin.State> {
  private _employees: Employee[];
  /*
  private _employees = [{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'front end engineer',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'naruto',
      lname: 'uzamaki',
      position: 'backend engineer',
      privileges: 'employee'
    }
  ];*/
  constructor(props?: Admin.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    const storage = storageHandler.verifyStoredData(props);
    this._employees = storage.employees;
    console.log(this._employees, 'what is this var???');
  }

  /*
  This screen will render a table of all the employees that should be loaded in the redux store
  */
  public render(): any {
    return (
      <section>
        <Header title={'Admin'} />
        <Table responsive={true}>
          <thead>
            <tr>
              <th>Id</th>
              <th>First Name</th>
              <th>Last Name</th>
              <th>Position</th>
            </tr>
          </thead>
          <tbody>
          {this._employees.map((employee: Employee) =>
            <tr key={employee.id}>
              <td>
                {employee.id}
              </td>
              <td>
                {employee.fname}
              </td>
              <td>
                {employee.lname}
              </td>
              <td>
                {employee.position}
              </td>
              <td>
                <Link to={`/employee/${employee.id}`}>View Employee</Link>
              </td>
            </tr>
          )}
          </tbody>
        </Table>
        <Button className={styles['btn-add-employee']} onClick={this._addEmployee} bsStyle="primary">Add Employee</Button>
      </section>
    );
  }

  /**
   * @desc adds new employee
   */
  private _addEmployee(): void {
    console.log(this.props, 'what is this props');
    this.props.history.push(`/employee/-/add`);
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._addEmployee = this._addEmployee.bind(scope);
  }
}
