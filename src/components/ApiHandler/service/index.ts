require('es6-promise').polyfill();
require('isomorphic-fetch');
import 'rxjs/add/observable/fromPromise';
import { Observable } from 'rxjs/Observable';

export class ApiHandler {

  /**
   * @desc handles api request which is wrapped in an rxjs Observable
   * @param url defines the url
   * @param requestData defines the request data
   * @param headers defines the request headers
   * @param action defines redux action to invoke
   * @param failAction defines a fail callback
   * @return Observerable that can be subscribed to listen to the fetch promise
   */
  static request(url: string, method: string, requestData?: any, headers = {}, action?: () => any, failAction?: () => any): Observable<any> {

    const requestInit: any = {
      method,
      headers: new Headers(headers),
      mode: 'cors'
    };

    const options: any = {
      method: 'OPTIONS',
      headers: new Headers(headers),
      mode: 'cors'
    };

    if (requestData) {
      requestInit.body = JSON.stringify(requestData);
    }

    if (action) {
      action();
    }

    return Observable.fromPromise(
      //first make a prefilight api call, to get approval from api server to complete api request
      fetch(url, options).then((response) => {
        if (!response.ok) {
          throw new Error('Network response was not ok.');
        }

        //complete api request and return data in json format
        return fetch(url, requestInit).then((fetchResponse) => {
          return fetchResponse.json();
        });
      }).catch((error) => {
        console.log('There has been a problem with your fetch operation: ' + error);
        if (failAction) {
          failAction();
        }
        return {
          error,
          ok: false
        };
      })
    );
  }

}
