import { clone as _clone, find as _find, get as _get} from 'lodash';
import * as React from 'react';
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandler, ApiHandlerActions, EmployeesActions, Header, LoginActions} from '../..';
import * as connectors from '../../../containers/connector';
import * as storageHandler from '../../../containers/storageHandler';
const styles = require('../styles/_reviews.scss');
const globalStyles = require('../../../containers/App/global.scss');

declare namespace ReviewsList {
  export interface Props extends RouteComponentProps<any> {
    apiActions?: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions: typeof LoginActions;
  }

  export interface State {
    status: string;
    addReview: boolean;
    reviewList: any;
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class ReviewsList extends React.Component<ReviewsList.Props, ReviewsList.State> {
  private _textInput: HTMLTextAreaElement;
  private _employee: any;
  private _canEdit: boolean;
  private _addReview: boolean = false;
  private _reviewerId: number;
  private _reviewsMap: any = {};
  private _employees; /*= [{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'frontend',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'naruto',
      lname: 'uzamaki',
      position: 'backend',
      privileges: 'employee'
    },
    {
      id: 2,
      fname: 'sasuke',
      lname: 'uchiha',
      position: 'devops',
      privileges: 'employee'
    }, {
      id: 3,
      fname: 'sakura',
      lname: 'haruno',
      position: 'qa',
      privileges: 'employee'
    }
  ];*/

  private _positions = {
    frontend: 'Frontend Engineer',
    backend: 'Backend Engineer',
    devops: 'Dev Ops',
    qa: 'QA',
    manager: 'Manager'
  };

  private _reviewList = [{
    id: 0,
    reviewerId: 0,
    review: 'hello world 0'
  }, {
    id: 1,
    reviewerId: 2,
    review: 'hello world 1'
  }];

  constructor(props?: ReviewsList.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    //default state
    this.state = {
      status: '',
      addReview: false,
      reviewList: this._reviewList
    };

    //check for valid employee or else redirect back to home screen
    const storage = storageHandler.verifyStoredData(props);

    //retrieve employees data from store
    this._employees = storage.employees;

    //check that current employee path is valid or else redirect user back to home
    this._employee = _find(this._employees, (o: any) => o.id === Number(this.props.match.params.id));
    //this._employees = _get(this.props, 'employeesState[0].employees.employees', []);
    //this._employee = _find(this._employees, (o: any) => o.id === Number(this.props.match.params.id));
    if (this._employee === undefined) {
      this.props.history.push('/');
    }
    //check for valid employee or else redirect back to home screen
    this._reviewerId = storage.user.id;
    //load all the reviews
    ApiHandler.request(`http://localhost:8080/api/reviews/${this._employee.id}?status=complete`, 'GET').subscribe((resp) => {
      this.setState({reviewList: resp });
    });

  }

  //display reviews, if current user created the review, they have access to update it
  public render(): any {
    return (
      <div>
        <Header title={'Reviews for ' + this._employee.fname + ' ' + this._employee.lname} />
        <section className={globalStyles['container-left-margin']}>
          {this.state.reviewList.map((reviews) =>
                  <div key={reviews.id}>
                      <label>Review by {this._getUserName(reviews.reviewerId)}</label><br/>
                      <textarea id={`review-${reviews.id}`} className={styles['textarea']} onChange={this._handleUpdate} defaultValue={reviews.review} disabled={reviews.reviewerId !== this._reviewerId} />
                      {(reviews.reviewerId === this._reviewerId) ? <span><br/><Button bsStyle="primary" data-id={reviews.id} className={styles['update-button']} onClick={this._updateReview.bind(this, reviews.id)}>Update</Button></span> : ''}
                  </div>
          )}
          <Button className={styles['btn-back']} onClick={this._goBack} bsStyle="primary" >Back</Button>
        </section>
      </div>
    );
  }

  private _getUserName(id): string {
    const employee = _find(this._employees, (o: any) => o.id === Number(id));
    return _get(employee, 'fname', '');
  }

  /**
   * @desc update the review
   * @param id defines the review id
   */
  private _updateReview(id): void {
    const request = {
      id,
      review: this._reviewsMap[`review-${id}`]
    };
    ApiHandler.request(`http://localhost:8080/api/reviews/${this._employee.id}`, 'PUT', request, {'Content-Type': 'application/json'}).subscribe((resp) => {
      console.log(resp, 'review has been updated');
    });
  }

  /**
   * @desc redirects to previous viewed page
   */
  private _goBack(): void {
    this.props.history.goBack();
  }

  /**
   * @desc stores each edit to textareas in object
   * @param e defines click event
   */
  private _handleUpdate(e): void {
    this._reviewsMap[e.target.id] = e.target.value;
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._goBack = this._goBack.bind(scope);
    this._handleUpdate = this._handleUpdate.bind(scope);
    this._getUserName = this._getUserName.bind(scope);
  }

}
