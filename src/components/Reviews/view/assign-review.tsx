import * as React from 'react';
import { Button } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandler } from '../../';

declare namespace ReviewsList {
  export interface Props {
    //testAction: (testState: TestState) => any;
    employeeId: number;
    employees: Employee[];
    closeModal?: () => void;
  }

  export interface State {
    employeesValue: number;
  }
}

export class AssignReview extends React.Component<ReviewsList.Props, ReviewsList.State> {
  private _employeeId: number;
  private _textInput: HTMLTextAreaElement;
  private _employee: any;
  private _canEdit: boolean;
  private _addReview: boolean = false;
  private _employees = [{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'frontend',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'naruto',
      lname: 'uzamaki',
      position: 'backend',
      privileges: 'employee'
    },
    {
      id: 2,
      fname: 'sasuke',
      lname: 'uchiha',
      position: 'devops',
      privileges: 'employee'
    }, {
      id: 3,
      fname: 'sakura',
      lname: 'haruno',
      position: 'qa',
      privileges: 'employee'
    }
  ];

  constructor(props?: ReviewsList.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    //default state
    this.state = {
      employeesValue: -1
    };
  }

  //renders review assignment screen
  public render(): any {
    return (
        <div>
          <span>Please Select User<br/><br/></span>
          <select
            onChange={this._selectEmployee}
          >
            <option value={-1}>--Employees--</option>
          {this.props.employees.map((employee) =>
              (employee.id !== this.props.employeeId) ?
              <option key={employee.id} value={employee.id}>{employee.fname + ' ' + employee.lname}</option>
              : ''
          )}
          </select>
          <br/>
          <br/>
          <Button onClick={this._saveAssignment} disabled={this.state.employeesValue === -1} bsStyle="primary">Save</Button>
        </div>
    );
  }

  /**
   * @desc determines of an employee has been selected or not
   */
  private _selectEmployee(e: any): void {
    this.setState({employeesValue: Number(e.target.value)});
  }

  /**
   * @desc save assignment of review; if this view is nested in a modal, the modal close callback can be triggered
   */
  private _saveAssignment(): void {
    const request = {
      employeeId: this.props.employeeId,
      reviewerId: this.state.employeesValue,
      status: 'pending'
    };
    ApiHandler.request('http://localhost:8080/api/review', 'POST', request, {'Content-Type': 'application/json'}).subscribe((resp) => {
      if (this.props.closeModal) {
        this.props.closeModal();
      }
    });
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._selectEmployee = this._selectEmployee.bind(scope);
    this._saveAssignment = this._saveAssignment.bind(scope);
  }
}
