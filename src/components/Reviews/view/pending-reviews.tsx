import { find as _find, get as _get} from 'lodash';
import * as React from 'react';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandler, ApiHandlerActions, EmployeesActions, Header, LoginActions} from '../..';
import * as connectors from '../../../containers/connector';
import * as storageHandler from '../../../containers/storageHandler';
import { Review } from './review';
const globalStyles = require('../../../containers/App/global.scss');

declare namespace PendingReviews {

  export interface Props extends RouteComponentProps<any> {
    apiActions?: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions: typeof LoginActions;
  }

  export interface State {
    pendingList: any[];
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class PendingReviews extends React.Component<any, PendingReviews.State> {
  private _employee: any;
  private _employees; /* = [{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'front end engineer',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'uzamaki',
      lname: 'naruto',
      position: 'backend engineer',
      privileges: 'employee'
    }
  ];

  private _pendingList = [{
    id: 0,
    employeeId:2,
    name: 'sasuke uchiha'
  }, {
    id: 1,
    employeeId:3,
    name: 'sakura haruno'
  }];*/

  constructor(props?: any, context?: any) {
    super(props, context);
    this._configBindings(this);
    const storage = storageHandler.verifyStoredData(props);

    //retrieve employees data from store
    this._employees = storage.employees;
    this.state = {
      pendingList: []
    };
    //check that current employee path is valid or else redirect user back to home
    this._employee = _find(this._employees, (o: any) => o.id === Number(this.props.match.params.id));
    ApiHandler.request(`http://localhost:8080/api/reviews/${this._employee.id}?status=pending`, 'GET').subscribe((resp) => {
      this.setState({pendingList: resp});
    });
  }

  //Renders pending review list
  public render(): any {
    return (
      <section>
        <Header title={'Pending Reviews'} />
        <table className={globalStyles['container-left-margin']}>
          <tbody>
            {this.state.pendingList.map((review) =>
              <tr key={review.id}>
                <td><Link to={`/employee/${review.reviewerId}/review`}>Complete Review for {this._getUserName(review.reviewerId)} </Link></td>
              </tr>
            )}
          </tbody>
        </table>
      </section>
    );
  }

  private _getUserName(id): string {
    const employee = _find(this._employees, (o: any) => o.id === Number(id));
    return _get(employee, 'fname', '');
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._getUserName = this._getUserName.bind(this);
  }
}
