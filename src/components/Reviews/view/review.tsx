import { find as _find, get as _get} from 'lodash';
import * as React from 'react';
import { Button, FormControl } from 'react-bootstrap';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandler, ApiHandlerActions, EmployeesActions, Header, LoginActions} from '../..';
import * as connectors from '../../../containers/connector';
import * as storageHandler from '../../../containers/storageHandler';
const styles = require('../styles/_reviews.scss');
const globalStyles = require('../../../containers/App/global.scss');

declare namespace Review {
  export interface Props extends RouteComponentProps<any> {
    //testAction: (testState: TestState) => any;
    apiActions?: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions: typeof LoginActions;
  }

  export interface State {
    status: string;
    addReview: boolean;
    isReviewTextEmpty;
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class Review extends React.Component<Review.Props, Review.State> {
  private _employee: any;
  private _employees: Employee[];
  /*[{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'frontend',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'naruto',
      lname: 'uzamaki',
      position: 'backend',
      privileges: 'employee'
    },
    {
      id: 2,
      fname: 'sasuke',
      lname: 'uchiha',
      position: 'devops',
      privileges: 'employee'
    }, {
      id: 3,
      fname: 'sakura',
      lname: 'haruno',
      position: 'qa',
      privileges: 'employee'
    }
  ];*/

  private _positions = {
    frontend: 'Frontend Engineer',
    backend: 'Backend Engineer',
    devops: 'Dev Ops',
    qa: 'QA',
    manager: 'Manager'
  };

  private _reviewerId;
  private _reviewText = '';

  constructor(props?: Review.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    //default state
    this.state = {
      status: '',
      addReview: false,
      isReviewTextEmpty: true
    };

    const storage = storageHandler.verifyStoredData(props);

    //the current person logged in is the reviewer
    this._reviewerId = storage.user.id;
    //check for valid employee or else redirect back to home screen
    this._employees = storage.employees;
    this._employee = _find(this._employees, (o: any) =>  o.id === Number(this.props.match.params.id));
    if (this._employee === undefined) {
      this.props.history.push(`/`);
    }
  }

  //<textarea ></textarea>

  //renders new review screen
  public render(): any {
    return (
      <div>
          <Header title={'New Review'} />
          <div className={globalStyles['container-left-margin']}>
            <FormControl className={styles['review-area']} onChange={this._handleInputChange} componentClass="textarea" placeholder="Enter review" />
            <Button onClick={this._goBack} className={globalStyles['btn-margin']} bsStyle="primary">Back</Button>
            <Button onClick={this._addReview} disabled={this.state.isReviewTextEmpty} bsStyle="primary">Save</Button>
          </div>
      </div>
    );
  }

  /**
   * @desc handles input change and gracefully disables save buttton if there is no input
   */
  private _handleInputChange(event: any): void {
    const target = event.target;
    const value = (target.valueParsed) ? target.valueParsed : target.value;
    const name = target.name;
    this._reviewText = value;
    if (this.state.isReviewTextEmpty && this._reviewText.length > 0) {
      this.setState({isReviewTextEmpty: false});
    } else if (!this.state.isReviewTextEmpty && this._reviewText.length === 0) {
      this.setState({isReviewTextEmpty: true});
    }
  }

  /**
   * @desc add a new completed review (admin only)
   */
  private _addReview(): void {
    const request = {
      employeeId: this._employee.id,
      reviewerId: this._reviewerId,
      status: 'complete',
      review: this._reviewText
    };

    ApiHandler.request('http://localhost:8080/api/review', 'POST', request, {'Content-Type': 'application/json'}).subscribe((resp) => {
      this.props.history.push(`/employee/${this._employee.id}`);
    });

  }

  /**
   * @desc redirects to last page viewed
   */
  private _goBack(): void {
    this.props.history.push(`/employee/${this._employee.id}`);
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._addReview = this._addReview.bind(scope);
    this._handleInputChange = this._handleInputChange.bind(scope);
    this._goBack = this._goBack.bind(this);
  }
}
