import { handleActions } from 'redux-actions';
import * as Actions from '../constants';

const initialState: TestState[] = [{
  status: 'good'
}];

export const testReducer = handleActions<TestState[], TestState>({
  [Actions.TEST]: (state, action) => {
    return [{
      status: action.payload.status
    }];
  }
}, initialState);