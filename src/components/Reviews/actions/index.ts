import { createAction } from 'redux-actions';
import * as Actions from '../constants';

export const testAction = createAction<TestState>(Actions.TEST);
