export { EmployeeView } from './view/employee-view';
export { EmployeeRoutes } from './view/employee-routes';
export { EmployeeDetails } from './view/employee-details';
export { employeesReducer } from './reducers';
import * as EmployeesActionsRef from './actions';
export const EmployeesActions = EmployeesActionsRef;