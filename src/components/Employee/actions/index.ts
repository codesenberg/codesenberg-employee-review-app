import { createAction } from 'redux-actions';
import * as Actions from '../constants';

export const storeEmployees = createAction<EmployeesState>(Actions.STORE_EMPLOYEES);
export const deleteEmployee = createAction<EmployeesState>(Actions.DELETE_EMPLOYEE);
export const addEmployee = createAction<EmployeesState>(Actions.ADD_EMPLOYEE);
export const updateEmployee = createAction<EmployeesState>(Actions.UPDATE_EMPLOYEE);