import { clone as _clone, find as _find, findIndex as _findIndex, get as _get, remove as _remove } from 'lodash';
import * as React from 'react';
import { Button, Modal} from 'react-bootstrap';
import { connect } from 'react-redux';
import { RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandler, ApiHandlerActions, AssignReview, EmployeesActions, Header, LoginActions, PendingReviews, ReviewsList} from '../..';
import * as connectors from '../../../containers/connector';
import * as storageHandler from '../../../containers/storageHandler';
import { EmployeeDetails } from './employee-details';
const styles = require('../styles/_employee-view.scss');
const globalStyles = require('../../../containers/App/global.scss');

declare namespace EmployeeView {
  export interface Props extends RouteComponentProps<any> {
    apiActions?: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions: typeof LoginActions;
  }

  export interface State {
    assignReview: boolean;
    deleteModalOpen: boolean;
    updateStatus: string;
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class EmployeeView extends React.Component<EmployeeView.Props, EmployeeView.State> {
  private _employee: any;
  private _isAdmin: boolean;
  private _updateInfo: any;
  private _employees;
  //for positions dropdown
  private _positions = {
    frontend: 'Frontend Engineer',
    backend: 'Backend Engineer',
    devops: 'Dev Ops',
    qa: 'QA',
    manager: 'Manager'
  };

  constructor(props?: EmployeeView.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    const storage = storageHandler.verifyStoredData(props);

    //retrieve employees data from store
    this._employees = storage.employees;

    //check that current employee path is valid or else redirect user back to home
    this._employee = _find(this._employees, (o: any) => o.id === Number(this.props.match.params.id));

    //setup details info store
    this._updateInfo = _clone(this._employee);

    //determines if current logged in user has admin priveleges
    this._isAdmin = storage.isAdmin;

    //initial states
    this.state = {
      assignReview: false,
      deleteModalOpen: false,
      updateStatus: ''
    };
  }

  //renders employee details; non-admin users may see details (but not edit) and their pending reviews requiring feedback
  //admin view allows admin user to edit user details, delete user, assign reviews, add their own new review and add new user; they will also be able to view user's reviews
  public render(): any {
    return (
      <section>
         <Header title={'Employee ' + this._employee.id} />
          <EmployeeDetails employee={this._employee} isEditabled={true} callback={this._handleUpdate}/>
          {(this._isAdmin) ?
             (<div className={globalStyles['container-left-margin']} >
              <div>{this.state.updateStatus}</div>
              <Button onClick={this._deleteModalPrompt} className={globalStyles['btn-margin']} bsStyle="primary">Delete</Button>
              <Button onClick={this._assignReviewOpen} className={globalStyles['btn-margin']} bsStyle="primary">Assign Review</Button>
              <Button onClick={this._updateUser} className={globalStyles['btn-margin']} bsStyle="primary">Update</Button>
              <Button onClick={this._addReview} bsStyle="primary">Add Review</Button>
            </div>) : ''
          }
        <div className={globalStyles['container-left-margin']}>
          <hr className={globalStyles.hr}/>
          {(this._isAdmin) ?
              <span>
                <Link to={`/employee/${this._employee.id}/reviews-list`}>View Completed Reviews for {this._employee.fname}</Link>
                <br/>
              </span>
            : ''}
          <Link to={`/employee/${this._employee.id}/pending-reviews`}>View Reviews Assigned to You</Link>
        </div>
        {(this._isAdmin) ?
          <div>
            <Modal
              show={this.state.assignReview}
              onHide={this._assignReviewClose}
            >
              <Modal.Header closeButton={true}>
                <Modal.Title>Assign User Review</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <AssignReview employeeId={this._employee.id} employees={this._employees} closeModal={this._assignReviewClose} />
              </Modal.Body>
            </Modal>
            <Modal
              show={this.state.deleteModalOpen}
              onHide={this._closeDeleteModalPrompt}
            >
              <Modal.Header closeButton={true}>
                <Modal.Title>Are you sure you want to delete?</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                  <Button onClick={this._closeDeleteModalPrompt} className={globalStyles['btn-margin']} bsStyle="primary">No</Button>
                  <Button onClick={this._deleteEmployee} bsStyle="danger">Yes</Button>
              </Modal.Body>
            </Modal>
          </div> : ''}
      </section>
    );
  }

  /**
   * @desc callback used to store updates from the EmployeeDetails component
   * @param key defines the url
   * @param value defines the request data
   */
  private _handleUpdate(key: any, value: any): void {
    this._updateInfo[key] = value;
  }

  /**
   * @desc open delete modal
   */
  private _deleteModalPrompt(): void {
    this.setState({deleteModalOpen: true});
  }

  /**
   * @desc close delete modal
   */
  private _closeDeleteModalPrompt(): void {
    this.setState({deleteModalOpen: false});
  }

  /**
   * @desc open assign review modal
   */
  private _assignReviewOpen(): void {
    this.setState({assignReview: true});
  }

  /**
   * @desc close assign review modal
   */
  private _assignReviewClose(): void {
    this.setState({assignReview: false});
  }

  /**
   * @desc redirect to add review page
   */
  private _addReview(): void {
    this.props.history.push(`/employee/${this._employee.id}/review`);
  }

  /**
   * @desc Delete user and close modal
   */
  private _deleteEmployee(): void {
    this.setState({deleteModalOpen: false});
    ApiHandler.request('http://localhost:8080/api/employees/' + this._employee.id, 'DELETE').subscribe((resp) => {
      _remove(this._employees, (o) => {
        return o.id === this._employee.id;
      });
      this.props.employeeActions.deleteEmployee({employees: this._employees});
      this.props.history.push(`/admin`);
    });
  }

  /**
   * @desc Updates user info
   */
  private _updateUser(): void {
    ApiHandler.request('http://localhost:8080/api/employees/2', 'PUT', this._updateInfo, {'Content-Type': 'application/json'}).subscribe((resp) => {
      this.setState({updateStatus: 'Updated.'});
      const idx = _findIndex(this._employees, (o) => {
        return o.id === Number(this.props.match.params.id);
      });
      this._employees[idx] = this._updateInfo;
      this.props.employeeActions.updateEmployee(this._employees);
    });
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._deleteEmployee = this._deleteEmployee.bind(scope);
    this._deleteModalPrompt = this._deleteModalPrompt.bind(scope);
    this._closeDeleteModalPrompt = this._closeDeleteModalPrompt.bind(scope);
    this._updateUser = this._updateUser.bind(scope);
    this._handleUpdate = this._handleUpdate.bind(scope);
    this._assignReviewOpen = this._assignReviewOpen.bind(scope);
    this._assignReviewClose = this._assignReviewClose.bind(scope);
    this._addReview = this._addReview.bind(scope);
  }
}
