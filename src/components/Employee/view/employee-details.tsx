import * as React from 'react';
import { ApiHandler } from '../../';
const styles = require('../styles/_employee-details.scss');
const globalStyles = require('../../../containers/App/global.scss');

declare namespace EmployeeDetails {
  export interface Props {
    employee?: Employee;
    isEditabled: boolean;
    callback?: (key, value) => any;
    showPrivilegeOption?: boolean;
  }

  export interface State {
    fname: string;
    lname: string;
    position: string;
  }
}

export class EmployeeDetails extends React.Component<EmployeeDetails.Props, EmployeeDetails.State> {
  private _employee: any;

  private _positions = {
    frontend: 'Frontend Engineer',
    backend: 'Backend Engineer',
    devops: 'Dev Ops',
    qa: 'QA',
    manager: 'Manager'
  };

  constructor(props?: EmployeeDetails.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    this._employee = (this.props.employee) ? this.props.employee : {};

    //initial state
    this.state = {
      fname: this._employee.fname,
      lname: this._employee.lname,
      position: this._employee.position
    };
  }

  //shows user details which can be configured to be editable
  public render(): any {
    return (
      <section className={[styles.details, globalStyles['container-left-margin']].join(' ')}>
          <label className={styles.label}>First Name: </label><br/>
          <input type="text" name="fname" onChange={this._handleInputChange} disabled={!this.props.isEditabled} defaultValue={this._employee.fname} /><br/>
          <label className={styles.label}>Last Name: </label><br/>
          <input type="text" name="lname" onChange={this._handleInputChange} disabled={!this.props.isEditabled} defaultValue={this._employee.lname} /><br/>
          <label className={styles.label}>Position: </label><br/>
          <select disabled={!this.props.isEditabled} name="position" defaultValue={this._employee.position} onChange={this._handleInputChange}>
            {Object.keys(this._positions).map((key) => {
                return <option key={key} value={key}>{this._positions[key]}</option>;
            })}
          </select>
          {(this.props.showPrivilegeOption) ?
          <div>
            <label className={styles.label}>Privilege: </label><br/>
            <select name="privilege" onChange={this._handleInputChange}>
                <option value={'employee'}>Employee</option>
                <option value={'admin'}>Admin</option>
            </select>
          </div>
          :''}
      </section>
    );
  }

  /**
   * @desc handles input change event
   * @param event defines change event
   */
  private _handleInputChange(event: any): void {
    const target = event.target;
    const value = (target.valueParsed) ? target.valueParsed : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
    if (this.props.callback) {
      this.props.callback(name, value);
    }
  }

  /**
   * @desc utility function bind class functions to the class scope
   * @param scope defines class scope
   */
  private _configBindings(scope: any): void {
    this._handleInputChange = this._handleInputChange.bind(scope);
  }
}
