import * as React from 'react';
import { Route, Switch } from 'react-router-dom';
import { PendingReviews, Review, ReviewsList } from '../..';
import { EmployeeAdd } from './employee-add';
import { EmployeeView} from './employee-view';

// Define all the employee/:id and its nested routes
export const EmployeeRoutes = () => (
  <Switch>
    <Route exact={true} path="/employee/:id"  component={EmployeeView} />
    <Route path="/employee/:id/reviews-list" component={ReviewsList} />
    <Route path="/employee/:id/pending-reviews" component={PendingReviews} />
    <Route path="/employee/:id/review" component={Review} />
    <Route path="/employee/:id/add" component={EmployeeAdd} />
  </Switch>
);