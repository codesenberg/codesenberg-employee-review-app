import * as React from 'react';
import { Button } from 'react-bootstrap';
import Modal from 'react-modal';
import { connect } from 'react-redux';
import { Redirect, RouteComponentProps } from 'react-router';
import { Link } from 'react-router-dom';
import { ApiHandler, ApiHandlerActions, AssignReview, EmployeesActions, Header, LoginActions, PendingReviews, ReviewsList } from '../..';
import * as connectors from '../../../containers/connector';
import * as storageHandler from '../../../containers/storageHandler';
import { EmployeeDetails } from './employee-details';
const styles = require('../styles/_employee-add.scss');
const globalStyles = require('../../../containers/App/global.scss');

declare namespace EmployeeAdd {
  export interface Props extends RouteComponentProps<any> {
    testAction: (testState: TestState) => any;
    apiActions?: typeof ApiHandlerActions;
    employeeActions: typeof EmployeesActions;
    apiState?: ApiRequestState;
    employeesState: EmployeesState;
    loginState: LoginState;
    loginActions: typeof LoginActions;
  }

  export interface State {
    userCreateStatus: string;
  }
}

@connect(connectors.mapStateToProps, connectors.mapDispatchToProps)
export class EmployeeAdd extends React.Component<EmployeeAdd.Props, EmployeeAdd.State> {
  private _textInput: HTMLInputElement;
  private _isAdmin: boolean;
  private _addInfo: Employee = {
    fname: '',
    lname: '',
    position: 'frontend',
    privileges: 'employee' };
  private _positions = {
    frontend: 'Frontend Engineer',
    backend: 'Backend Engineer',
    devops: 'Dev Ops',
    qa: 'QA',
    manager: 'Manager'
  };
  private employees: Employee[];

  constructor(props?: EmployeeAdd.Props, context?: any) {
    super(props, context);
    this._configBindings(this);
    const storage = storageHandler.verifyStoredData(props);
    this.employees = storage.employees;
    //default state
    this.state = {
      userCreateStatus: ''
    };
  }

  //render screen that allows admin user to add new employee
  public render(): any {
    return (
      <section>
        <Header title={'Add New Employee'} />
        <EmployeeDetails isEditabled={true} callback={this._handleUpdate} showPrivilegeOption={true}/>
        <div className={globalStyles['container-left-margin']}>
          <Button onClick={this.props.history.goBack} className={globalStyles['btn-margin']} bsStyle="primary">Back</Button>
          <Button onClick={this._addUser} bsStyle="primary">Add User</Button>
          {(this.state.userCreateStatus !== '') ? <div className={styles.status}>{this.state.userCreateStatus}</div> : ''}
        </div>
      </section>
    );
  }
 //<Button onClick={() => this.setState({deleteModalOpen: true})} className={globalStyles['btn-margin']} bsStyle="primary">Delete</Button>
  /**
   * @desc creates new employee and redirects to that page after 3 seconds on successful creation
   */
  private _addUser(): void {
    ApiHandler.request('http://localhost:8080/api/employees', 'POST', this._addInfo, {'Content-Type': 'application/json'}).subscribe((resp) => {
        if (resp.response === 'ok') {
          this.setState({userCreateStatus: 'Your user has been created. Redirecting to new user.'});
          this._addInfo.id = resp.employeeId;
          this.employees.push(this._addInfo);
          this.props.employeeActions.addEmployee({employees: this.employees});
          setTimeout(() => {
            this.props.history.push(`/admin`);
            //this.props.history.push(`/employee/${resp.employeeId}`);
          }, 3000);
        } else {
          this.setState({userCreateStatus: 'User creation failed'});
        }
    });
  }

  /**
   * @desc callback used to store updates from the EmployeeDetails component
   * @param key defines the url
   * @param value defines the request data
   */
  private _handleUpdate(key: any, value: any): void {
    this._addInfo[key] = value;
  }

  /**
   * @desc Delete user and close modal
   */
  private _configBindings(scope: any): void {
    this._handleUpdate = this._handleUpdate.bind(scope);
    this._addUser = this._addUser.bind(scope);
  }
}
