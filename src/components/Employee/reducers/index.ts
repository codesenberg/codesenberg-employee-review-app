
import { handleActions } from 'redux-actions';
import * as Cookies from 'universal-cookie';
import * as Actions from '../constants';

const cookies = new Cookies();
const initialState: EmployeesState[] = [{
  //Mock default data
  employees: []
  /*employees: [{
      id: 0,
      fname: 'kakashi',
      lname: 'hatake',
      position: 'frontend',
      privileges: 'admin'
    },
    {
      id: 1,
      fname: 'naruto',
      lname: 'uzamaki',
      position: 'backend',
      privileges: 'employee'
    },
    {
      id: 2,
      fname: 'sasuke',
      lname: 'uchiha',
      position: 'devops',
      privileges: 'employee'
    }, {
      id: 3,
      fname: 'sakura',
      lname: 'haruno',
      position: 'qa',
      privileges: 'employee'
    }, {
      id: 23,
      fname: 'test',
      lname: 'haruno',
      position: 'qa',
      privileges: 'employee'
    }
  ]*/
}];

export const employeesReducer = handleActions<EmployeesState[], EmployeesState>({
  //load in employees info into store so it can be referenced throughout the app
  [Actions.STORE_EMPLOYEES]: (state, action) => {
    return [{
      employees: action.payload
    }];
  },
  //delete employee from store
  [Actions.DELETE_EMPLOYEE]: (state, action) => {
    const result = {
      employees: action.payload
    };
    cookies.remove('cea-employees');
    cookies.set('cea-employees', result);
    return [result];
  },
  //add employee to store
  [Actions.ADD_EMPLOYEE]: (state, action) => {
    const result = {
      employees: action.payload.employees
    };
    cookies.remove('cea-employees');
    cookies.set('cea-employees', result);
    return [result];
  },
  //add employee to store
  [Actions.UPDATE_EMPLOYEE]: (state, action) => {
    const result = {
      employees: action.payload
    };
    cookies.remove('cea-employees');
    cookies.set('cea-employees', result);
    return [result];
  }

}, initialState);