/**
 * Combine all reducers to one root reducer for the redux store
 */
import { combineReducers } from 'redux';
import { apiHandlerReducer, employeesReducer, loginReducers } from '../components';

export interface RootState {
    apiHandlerReducer: ApiRequestState[];
    employeesReducer: EmployeesState[];
    loginReducers: LoginState[];
}

export default combineReducers<RootState>({
    apiHandlerReducer,
    employeesReducer,
    loginReducers
});
