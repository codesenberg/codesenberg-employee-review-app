/**
 * TodoMVC model definitions
 */

declare interface TestState {
  status: string;
}

declare interface Employee {
  id?: number;
  fname: string;
  lname: string;
  position: string;
  privileges: string;
}
declare interface EmployeesState {
  employees: Employee[];
}

declare interface LoginState {
  user: Employee;
}

declare interface ApiRequestState {
  errorStatus: number;
  isLoading: boolean;
  response: any[];
}

declare interface StorageHandlerParams {
  loginState: any;
  loginActions: any;
  history: any;
  employeesState: any;
  employeeActions: any;
  location: any;
}

declare interface StorageHandler {
  user: Employee;
  employees: Employee[];
  isAdmin: boolean;
}