/**
 * Initialize the redux store by passing in the initial state and combined reducers
 */

import { applyMiddleware, createStore, Store } from 'redux';
import { logger } from '../middleware';
import rootReducer, { RootState } from '../root-reducer';

export function configureStore(initialState?: RootState): Store<RootState> {
  const create = window.devToolsExtension
    ? window.devToolsExtension()(createStore)
    : createStore;

  const createStoreWithMiddleware = applyMiddleware(logger)(create);

  const store = createStoreWithMiddleware(rootReducer, initialState) as Store<RootState>;

  if (module.hot) {
    module.hot.accept('../root-reducer', () => {
      const nextReducer = require('../root-reducer');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}
