import { createBrowserHistory } from 'history';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Route, Router, Switch } from 'react-router';
import { Admin, EmployeeRoutes, Login } from './components';
import { App } from './containers/App';
import { configureStore } from './store';

const store = configureStore();
const history = createBrowserHistory();

//setup the base routes for this app which are admin and employee
ReactDOM.render(
  <Provider store={store}>
    <Router history={history}>
      <Switch>
        <Route exact={true} path="/" component={App} />
        <Route path="/login" component={Login} />
        <Route path="/admin" component={Admin} />
        <Route path="/employee/:id"  component={EmployeeRoutes} />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
